package app.controllers;

import app.Application;
import app.models.User;
import app.models.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class UserController {
    @Autowired
    private UserDao userDao;
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    /**
     * / --> Главнав форма сайта.
     * @param filter Текст по которому фильтруем данные
     * @param page Страница просмотра результата
     */
    @RequestMapping("/")
    public String greeting(
            @RequestParam(value="filter", required=false, defaultValue="") String filter,
            @RequestParam(value="page", required=false, defaultValue="0") int page,
            Model model
    ) {
        Pageable paginator = new PageRequest(page, 5);
        Page<User> usersPage;
        if((filter != null)&&(filter.length()>0)){
            usersPage = userDao.findByName(filter, paginator);
        }else{
            usersPage = userDao.findAll(paginator);
        }

        model.addAttribute("users", usersPage.getContent());

        if(usersPage.hasPrevious()) {
            model.addAttribute("prevPage", paginator.getPageNumber()-1);
        }
        if(usersPage.hasNext()) {
            model.addAttribute("nextPage", paginator.getPageNumber()+1);
        }

        model.addAttribute("currentPage", Integer.toString(paginator.getPageNumber()));
        model.addAttribute("filter", filter);

        return "greeting";
    }

    /**
     * /create  --> Форма создания пользователя.
     */
    @RequestMapping(value = "/create", method = {RequestMethod.GET})
    public String create(Model model) {
        return "user";
    }

    /**
     * /create  --> Создаем нового пользователя.
     * @param name User's name
     * @param age Integer age
     * @param isAdmin Byte isAdmin
     * @return Строка которая описывает результат.
     */
    @RequestMapping(value = "/create", method = {RequestMethod.POST})
    public String create(String name, Integer age, Byte isAdmin) {
        User user = null;
        try {
            user = new User(name, age, isAdmin);
            userDao.save(user);
        }
        catch (Exception ex) {
            log.error("Ошибка при создании пользователя", ex);
            return "redirect:/";
        }
        return "redirect:update?created=true&&id=" + user.getId() ;
    }

    /**
     * /delete  --> Показваем форму пользователя с конкретным id дял удаления.
     *
     * @param id id удаляемого пользоваетля
     * @return Форма которая описывает результат.
     */
    @RequestMapping(value = "/delete", method = {RequestMethod.GET})
    public String getDelete(long id, Model model) {
        try {
            User user = userDao.findOne(id);
            if(user!=null) {
                setModelData(model, user);
            }else{
                model.addAttribute("error", "Пользователь с id = " + id + " не существует!!!");
            }
        }
        catch (Exception ex) {
            log.error("Ошибка при удалении пользователя с id= " + id, ex);
            model.addAttribute("error", "Ошибка при удалении пользователя с id = " + id + ex.toString());
        }
        return "delete_user";
    }

    /**
     * /delete  --> Удаляем пользователя с конкретным id.
     *
     * @param id id удаляемого пользоваетля
     * @return Форма которая описывает результат.
     */
    @RequestMapping(value = "/delete", method = {RequestMethod.POST})
    public String postDelete(long id, Model model) {
        try {
            User user = userDao.findOne(id);
            if(user!=null) {
                setModelData(model, user);
                userDao.delete(user);
                model.addAttribute("deleted", "Пользователя с id = " + id + " удален!!!");
            }else{
                model.addAttribute("error", "Пользователя с id = " + id + " не существует!!!");
            }
        }
        catch (Exception ex) {
            log.error("Ошибка при удалении пользователя с id= " + id, ex);
            model.addAttribute("error", "Ошибка при удалении пользователя с id = " + id + ex.toString());
        }

        return "delete_user";
    }

    /**
     * /update  --> Обновляем пользователя с конкретным id.
     *
     * @param id id обновляемого пользоваетля
     * @return Форма которая описывает результат.
     */
    @RequestMapping(value = "/update", method = {RequestMethod.GET})
    public String updateUser(@RequestParam(value="created", required=false, defaultValue="false") boolean created, long id, Model model) {
        try {
            User user = userDao.findOne(id);
            if(user!=null) {
                setModelData(model, user);
            } else {
                model.addAttribute("error", "Пользователя с id = " + id + " не существует!!!");
            }
            if(created){
                model.addAttribute("success_created", "Пользователя создан с id = " + id + "!!!");
            }
        }
        catch (Exception ex) {
            log.error("Ошибка при обновлении пользователя с id= " + id, ex);
            model.addAttribute("error", "Ошибка при обновлении пользователя с id = " + id + ex.toString());
        }
        return "user";
    }

    /**
     * /update  --> Обновляем пользователя с конкретным id.
     *
     * @param id The id for the user to update.
     * @param name User's new name
     * @param age Integer new age
     * @param isAdmin Byte new isAdmin
     * @return Форма которая описывает результат.
     */
    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public String updateUser(long id, String name, Integer age, Byte isAdmin, Model model) {
        try {
            User user = userDao.findOne(id);
            if(user!=null) {
                user.setName(name);
                user.setAge(age);
                user.setIsAdmin(isAdmin);
                userDao.save(user);
                setModelData(model, user);
            } else {
                model.addAttribute("error", "Пользователя с id = " + id + " не существует!!!");
            }
        }
        catch (Exception ex) {
            log.error("Ошибка при обновлении пользователя с id= " + id, ex);
            model.addAttribute("error", "Ошибка при обновлении пользователя с id = " + id + ex.toString());
        }
        return "user";
    }

    /**
     * Передаем данные модели в контескт для отрисовки.
     */
    public void setModelData(Model model, User user){
        model.addAttribute("id", user.getId());
        model.addAttribute("name", user.getName());
        model.addAttribute("age", user.getAge());
        model.addAttribute("isAdmin", user.getIsAdmin());
        model.addAttribute("createdDate", user.getCreatedDate());
    }
}