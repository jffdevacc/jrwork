package app.models;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "users")
public class User {
    // ------------------------
    // PRIVATE FIELDS
    // ------------------------

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    // The user's name
    private String name;

    // The user's age
    private Integer age;

    // The user's isAdmin
    private Byte isAdmin;

    // The user's createdDate
    private Date createdDate;

    // ------------------------
    // PUBLIC METHODS
    // ------------------------

    public User() { }

    public User(long id) {
        this.id = id;
    }

    public User(String name, Integer age, Byte isAdmin) {
        this.name = name;
        this.age = age;
        this.isAdmin = isAdmin;
    }
    // Getter and setter methods

    public long getId() {
        return id;
    }

    public void setId(long value) {
        this.id = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setIsAdmin(Byte isAdmin) {
        this.isAdmin = isAdmin;
    }

    public Byte getIsAdmin() {
        return isAdmin;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    @PrePersist
    protected void onCreate() {
        createdDate = new Date();
    }

}