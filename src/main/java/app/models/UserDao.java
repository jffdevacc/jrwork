package app.models;


import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

@Transactional
public interface UserDao extends CrudRepository<User, Long> {

    Page<User> findByName(String name, Pageable pageable);
    Page<User> findAll(Pageable pageable);
}